
public class Konfiguration_Chaos {

	public static void main(String[] args) {
		//kommentar
		int euro;
		int cent;
		int summe;
		int muenzenEuro = 130;
		int muenzenCent = 1280;
		double fuellstand;
		double patrone = 46.24;
		final double maximum = 100.00;
		char sprachModul = 'd';
		byte PRUEFNR = 4;
		boolean statusCheck;
		String typ = "Automat AVR";
		String bezeichnung = "Q2021_FAB_A";
		String name;

		name = typ + " " + bezeichnung;
		fuellstand = maximum - patrone;
		summe = muenzenCent + muenzenEuro * 100;
		euro = summe / 100;
	    cent = summe % 100;
		statusCheck = (euro <= 150) 
					&& (euro >= 50)	
					&& (cent != 0) 
					&& (sprachModul == 'd')
					&& (fuellstand >= 50.00) 
					&&  (!(PRUEFNR == 5 || PRUEFNR == 6)); 

		
		System.out.println("Name: " + name);
		System.out.println("Sprache: " + sprachModul);
		System.out.println("Pruefnummer : " + PRUEFNR);
		System.out.println("Fuellstand Patrone: " + fuellstand + " %");
		System.out.println("Summe Euro: " + euro +  " Euro");
		System.out.println("Summe Rest: " + cent +  " Cent");		
		System.out.println("Status: " + statusCheck);
	}
}
